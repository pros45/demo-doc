# Liste des eleves

## Ordre croissant

* BELIN, yannick
* CHAUBARD, Stéphanie
* FAUVERGUE, Nicolas
* KAPGNEP Flora
* PERRET, Bertrand
* ROSSER Paul
* ROLLAND, Glenn
* THIBAULT, Stéphane

## Ordre décroissant

* ROSSER Paul
* PERRET, Bertrand
* ROLLAND, Glenn
* PERRET, Bertrand
* FAUVERGUE, Nicolas
* CHAUBARD, Stéphanie
* BELIN, Yannick
